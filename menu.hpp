#ifndef __CONSOLE_MENU_HPP__
#define __CONSOLE_MENU_HPP__

#include <string>
#include <vector>
#include <map>

#include <getkey.hpp>

class Menu{

    friend class Option;

    private:

        class Option{

            friend class Menu;

            private:
                std::string name;
                Key shortcut = Key::UNDEFINED;
                std::string value;
                bool invisible = false;


            public:
                Option(const std::string name, const Key shortcut, const std::string value);
                Option(const std::string name, const Key shortcut);
                Option(const Key shortcut, const std::string value);
                Option(const std::string name);
                Option() = delete;

                ~ Option() = default;

                inline std::string getName(){
                    return name;
                }
                inline Key getShortcut(){
                    return shortcut;
                }
                inline std::string getValue(){
                    return value;
                }
        };

        std::string name = "Menu";
        std::vector<Option> options = {};
        unsigned selected = 0;
        bool valid_input = false;

        std::vector<Option> __Options__(bool _v);

        std::string create_Window();
        void erase();
        void action(const Key key);
        inline std::vector<Option> VisibleOptions(){
            return Menu::__Options__(false);
        }

        inline std::vector<Option> InvisibleOptions(){
            return Menu::__Options__(true);
        }

    public:
        Menu(const std::string name, const std::vector<Option> options);
        Menu(const std::string name);
        Menu();


        inline void addOption(const Option option){
            options.push_back(option);
        }

        void addOption(const Key shortcut, const std::string name);
        void addOption(const Key shortcut, const std::string name, const std::string value);

        void bind(const Key shortcut, const std::string value);

        void removeOption(const std::string name);
        inline void removeOption(const int index){
            options.erase(options.begin() + index);
        }

        std::string show();

        void reset();

        inline std::string getName(){
            return name;
        }

        inline std::vector<Option> getOptions(){
            return options;
        }

        inline unsigned getSelected(){
            return selected;
        }

        inline bool isRunning(){
            return !valid_input;
        }
};



class Input{

    public:
        enum Type{
                TEXT,
                PASSWORD,
                INTEGER,
                DECIMAL
            };

    private:
        std::string name = "Input (type=%t)";
        std::string value = "";
        std::string display = "";
        char placeholder = '*';
        Type type = Type::TEXT;
        unsigned max_length = 50;
        unsigned cursor_position = 0;
        bool valid_input = false;

        std::string create_Window();

        void erase();

        void action(const Key key);

        std::string titleProcessor();

        void addChar(const char& c);

        void deleteChar(unsigned pos);


    public:

        Input(const std::string name, const Type type, const unsigned max_length);
        Input(const std::string name, const Type type);
        Input(const std::string name);
        Input();


        inline std::string getRawTitle(){
            return name;
        }

        inline std::string getTitle(){
            return titleProcessor();
        }

        inline std::string getValue(){
            return value;
        }

        inline std::string getDisplay(){
            return display;
        }

        inline Type getType(){
            return type;
        }

        inline unsigned getMaxLength(){
            return max_length;
        }

        std::string show();
};



class Information{
    private:
        std::string message = "Information";
        unsigned max_length = 50;
        bool valid_input = false;
        unsigned nb_lines = 0;

        std::string create_Window();

        void erase();

        bool action(const Key key);

    public:

        Information(const std::string message, const unsigned max_length);
        Information(const std::string message);
        Information() = delete;


        inline std::string getTitle(){
            return message;
        }

        void show();

        inline bool isRunning(){
            return !valid_input;
        }

        inline void reset(){
            valid_input = false;
        }

        inline void setMaxLength(const unsigned max_length){
            this->max_length = max_length;
        }

        inline unsigned getMaxLength(){
            return max_length;
        }

        inline unsigned getNbLines(){
            return nb_lines;
        }

        inline std::string getMessage(){
            return message;
        }
};




class Choice{
    private:
        std::string title;
        std::string option1;
        std::string option2;
        bool is_2_selected;

    public:
        Choice(std::string title, std::string option1, std::string option2, bool default_value);
        Choice(std::string title, bool default_value = 0);
        Choice() = delete;

        inline std::string getTitle(){
            return title;
        }
        inline std::string getOption1(){
            return option1;
        }
        inline std::string getOption2(){
            return option2;
        }
        inline bool isOption2Selected(){
            return is_2_selected;
        }

        std::string create_Window();

        bool show();

        void erase();
};

#endif