#include "menu.hpp"

#include "cutility.hpp"
#include "getkey.hpp"

#include <cmath>
#include <iostream>

#include <ansi.h>
#include <utf-8.h>
#include <raw_mode.h>

// ----------------------------------------------------------------------
// ---------------------------- Menu::Option ----------------------------


Menu::Option::Option(const std::string name, const Key shortcut, const std::string value)
: name(name), shortcut(shortcut), value(value) {}

Menu::Option::Option(const std::string name, const Key shortcut)
: name(name), shortcut(shortcut), value(name) {}

Menu::Option::Option(const Key shortcut, const std::string value) //shortcut only, will not be displayed
: name(value), shortcut(shortcut), value(value), invisible(true) {}

Menu::Option::Option(const std::string name)
: name(name), shortcut(Key::UNDEFINED), value(name) {}

// ----------------------------------------------------------------------
// -------------------------------- Menu --------------------------------

Menu::Menu(const std::string _name, const std::vector<Option> _options)
: name(_name), options(_options) {
    enable_raw_mode();
    enableANSI();
    enableUTF8();
}

Menu::Menu(const std::string name)
: Menu(name, {}) {}

Menu::Menu()
: Menu("Menu", {}) {}

std::vector<Menu::Option> Menu::__Options__(bool _v){
    std::vector<Menu::Option> _options = {};
    for(unsigned i = 0; i < options.size(); i++){
        if(options[i].invisible == _v){
            _options.push_back(options[i]);
        }
    }
    return _options;
}


void Menu::addOption(const Key shortcut, const std::string name){
    options.push_back(Option(name, shortcut));
}

void Menu::addOption(const Key shortcut, const std::string name, const std::string value){
    options.push_back(Option(name, shortcut, value));
}

void Menu::bind(const Key shortcut, const std::string value){
    options.push_back(Option(shortcut, value));
}

void Menu::removeOption(const std::string name){
    for(unsigned i = 0; i < options.size(); i++){
        if(options[i].name == name){
            options.erase(options.begin() + i);
            return;
        }
    }
}

void Menu::action(const Key key){
    switch (key){
        case Key::ARROW_UP:{
            if(selected > 0){
                selected--;
            }
            break;                            
        }
        case Key::ARROW_DOWN:{
            if(selected < VisibleOptions().size() - 1){
                selected++;
            }
            break;
        }
        case Key::ENTER:{
            valid_input = true;
            break;
        }
        default:{ //if the input is a shortcut
            for(std::size_t i = 0; i < options.size(); i++){
                if(options[i].shortcut == key){
                    selected = i;
                    valid_input = true;
                    break;
                }
            }
        }
    }
}

std::string Menu::create_Window(){
    std::string window = "";

    unsigned width = Utility::getNbchars(name);
    for(Option option : VisibleOptions()){
        if(!option.invisible){
            if(option.name.size() > width){
                width = Utility::getNbchars(option.name);
            }
        }
    }
    width += 6;

    //make a rectangle around the menu
    window += "┌";
    for(std::size_t i = 0; i < width; i++){
        window += "─";
    }
    window += "┐\n";

    //print the title
    window += "│ ";

    unsigned short spaces = round(double(width - Utility::getNbchars(name) - 2)/2);
    for(std::size_t i = 0; i < spaces; i++){
        window += " ";
    }

    window += name + " ";
    for(std::size_t i = 0; i < width-(spaces+Utility::getNbchars(name))-2; i++){
        window += " ";
    }
    window += "│\n";

    window += "├───┬";
    for(std::size_t i = 0; i < width-4; i++){
        window += "─";
    }
    window += "┤\n";

    //print the options
    for(std::size_t i = 0; i < VisibleOptions().size(); i++){
        Option option = VisibleOptions()[i];
        if(option.invisible){
            continue;
        }
        window += "│ ";
        window += Getkey::getchar(option.shortcut);
        window += " │ ";
        if(i == selected){
            window += "\x1B[7m\x1B[37m";
        }
        window += option.name;
        if(i == selected){
            window += "\x1B[0m";
        }
        window += " ";


        unsigned short missing_char = width - Utility::getNbchars(option.name)- 6;
        for(std::size_t j = 0; j < missing_char; j++){
            window += " ";
        }
        window += "│\n";
    }
    window += "└───┴";
    for(std::size_t i = 0; i < width-4; i++){
        window += "─";
    }
    window += "┘\n";

    return window;
}

std::string Menu::show(){
    do{
        std::cout << create_Window() << std::flush;
        Key c = Getkey::getkey();
        action(c);
        erase();
    }while(!valid_input);
    return options[selected].value;
}

void Menu::erase(){
    std::cout << "\x1B[" << VisibleOptions().size() + 4 << "A" << "\x1B[0J" << std::flush;
}

void Menu::reset(){
    selected = 0;
    valid_input = false;
}

// ----------------------------------------------------------------------
// -------------------------------- Input -------------------------------

Input::Input(const std::string _name, Input::Type _type, const unsigned _max_length)
: name(_name), type(_type), max_length(_max_length) {
    enable_raw_mode();
    enableANSI();
    enableUTF8();}

Input::Input(const std::string name, Input::Type type)
: Input(name, type, 0) {}

Input::Input(const std::string name)
: Input(name, Input::Type::TEXT, 0) {}

Input::Input()
: Input("", Input::Type::TEXT, 0) {}


std::string Input::create_Window(){
    
    unsigned width = Utility::getNbchars(name);
    if(width < max_length){
        width = max_length;
    }

    std::string window = "";
    window += "┌";
    for (std::size_t i = 0; i < width + 2; i++){
        window += "─";
    }
    window += "┐\n│ ";  

    std::string title = titleProcessor();
    window += Utility::extend(title, width + 1, ' ', true);

    window += "│\n│ ";
    //write display, with the cursor at cursor_position; add underscore to fill the rest of the window; keep 4 spaces at the beginning and at the end
    for (std::size_t i = 0; i < width; i++){
        if (i == cursor_position){ // cursor, invert colors
            window += "\x1B[7m";
            if(i < Utility::getNbchars(display)){
                window += display[i];
            }
            else{
                window += "_";
            }
            window += "\x1B[0m";
        }
        else{
            if(i < Utility::getNbchars(display)){
                window += display[i];
            }
            else{
                window += "_";
            }
        }
    }
    
    window += " │\n└";
    for (std::size_t i = 0; i < width + 2; i++){
        window += "─";
    }
    window += "┘";
    return window;
}

//verify if the input is valid for the type of input, and add the character to the value
void Input::addChar(const char& c){
    if(value.size() < max_length){
        switch (type){
            case TEXT:{
                value.insert(cursor_position, 1, c);
                display.insert(cursor_position, 1, c);
                cursor_position++;
                break;
            }
            case PASSWORD:{
                value.insert(cursor_position, 1, c);
                display.insert(cursor_position, 1, '*');
                cursor_position++;
                break;
            }
            case INTEGER:{
                if ((c >= '0' && c <= '9') || (c == '-' && value.size() == 0)){
                    value.insert(cursor_position, 1, c);
                    display.insert(cursor_position, 1, c);
                    cursor_position++;
                }
                break;
            }
            case DECIMAL:{
                if ((((c >= '0' && c <= '9') || c == '.') && (value.find('.') == std::string::npos || c != '.')) || (c == '-' && value.size() == 0)){
                    value.insert(cursor_position, 1, c);
                    display.insert(cursor_position, 1, c);
                    cursor_position++;
                }
                break;
            }
        }
    }
}

void Input::deleteChar(unsigned pos){
    value.erase(pos, 1);
    display.erase(pos, 1);
    value.shrink_to_fit();
    display.shrink_to_fit();
}

void Input::action(const Key c){
    switch (c){
        case Key::ENTER:{
            valid_input = true;
            if (Utility::getNbchars(value) == 0){
                value = display = ' ';
            }
            break;
        }
        case Key::BACKSPACE:{
            if (cursor_position > 0){
                deleteChar(cursor_position-1);
                cursor_position--;
            }
            break;
        }
        case Key::_DELETE:{
            if (cursor_position < Utility::getNbchars(value)){
                deleteChar(cursor_position);
            }
            break;
        }
        case Key::ARROW_LEFT:{
            if (cursor_position > 0){
                cursor_position--;
            }
            break;
        }
        case Key::ARROW_RIGHT:{
            if (cursor_position < Utility::getNbchars(value)){
                cursor_position++;
            }
            break;
        }
        case Key::HOME:{
            cursor_position = 0;
            break;
        }
        case Key::END:{
            cursor_position = value.size();
            break;
        }
        case Key::ESCAPE:{
            valid_input = true;
            value = display = "";
            break;
        }

        default:{
            if (c != Key::UNKNOWN){
                addChar(Getkey::getchar(c));
            }
            break;
        }
    }
}

void Input::erase(){
    std::cout << "\x1B[4A\x1B[0J" << std::flush;
}

std::string Input::show(){
    do{
        std::cout << create_Window() << std::endl;
        Key c = Getkey::getkey();
        action(c);
        erase();
    }while(!valid_input);
    return getValue();
}

std::string Input::titleProcessor(){
    std::string _title = name;
    for (std::size_t i = 0; i < Utility::getNbchars(name); ++i){
        if (_title[i] == '%'){
            if (_title[i + 1] == 't'){
                switch (type){
                    case TEXT:
                        _title.replace(i, 2, "text");
                        break;
                    case PASSWORD:
                        _title.replace(i, 2, "password");
                        break;
                    case INTEGER:
                        _title.replace(i, 2, "number");
                        break;
                    case DECIMAL:
                        _title.replace(i, 2, "decimal");
                        break;

                }
            }
            else if (_title[i + 1] == 'm'){
                _title.replace(i, 2, std::to_string(max_length));

            }
        }
    }
    return _title;
}

// ---------------------------------------------------------------------
// -------------------------- Information ------------------------------

Information::Information(const std::string _message, const unsigned _max_length)
: message(_message), max_length(_max_length)
{
    if (max_length < 10){
        this->max_length = 10;
    }

    nb_lines = 1;
    if (Utility::getNbchars(message) > max_length){
        std::vector<std::string> lines = Utility::split(message, size_t(max_length));
        message = Utility::join(lines,"\n");
        nb_lines = lines.size();
    }

    enable_raw_mode();
    enableANSI();
    enableUTF8();
}

Information::Information(const std::string _message)
:Information(_message, 50)
{}


std::string Information::create_Window(){
    unsigned larger = 0;
    std::vector<std::string> lines = Utility::split(message, '\n');
    for (std::string s : lines){
        if (Utility::getNbchars(s) > larger){
            larger = Utility::getNbchars(s);
        }
    }

    std::string window = "┌";
    for (std::size_t i = 0; i < larger + 2; i++){
        window += "─";
    }
    window += "┐\n";

    for (std::string s : lines){
        window += "│ ";
        window += Utility::extend(s, larger);
        window += " │\n";
    }

    window += "└";
    for (std::size_t i = 0; i < larger + 2; i++){
        window += "─";
    }
    window += "┘";

    return window;
}

void Information::show(){
    std::cout << create_Window() << std::endl;
    Getkey::waitForKey();
    erase();
}

void Information::erase(){
    std::cout << "\x1B[" << nb_lines+2 << "A\x1B[0J" << std::flush;
}

// ---------------------------------------------------------------------
// ----------------------------- Choice --------------------------------


Choice::Choice(std::string _title, std::string _option1, std::string _option2, bool default_value)
: title(_title), option1(_option1), option2(_option2), is_2_selected(default_value)
{}

Choice::Choice(std::string title, bool default_value)
: Choice(title, "Yes", "No", default_value)
{}

std::string Choice::create_Window(){
    unsigned width = Utility::getNbchars(title);
    width < (Utility::getNbchars(option1) + 3 + Utility::getNbchars(option2)) ?
    width = Utility::getNbchars(option1) + 3 + Utility::getNbchars(option2) :
    width = width;

    unsigned width_opt1 = (width-3)/2;
    unsigned width_opt2 = width-3-width_opt1;

    std::string window = "┌";
    for (std::size_t i = 0; i < width + 2; i++){
        window += "─";
    }
    window += "┐\n";

    window += "│ "+Utility::extend(title, width, ' ', true) + " │\n";

    window += "│ ";
    if(!is_2_selected){
        window += "\x1B[7m\x1B[37m";
    }
    window += Utility::extend(option1, width_opt1, ' ', true);
    if(!is_2_selected){
        window += "\x1B[0m";
    }
    window += " │ ";
    if(is_2_selected){
        window += "\x1B[7m\x1B[37m";
    }
    window += Utility::extend(option2, width_opt2, ' ', true);
    if(is_2_selected){
        window += "\x1B[0m";
    }
    window += " │\n";

    window += "└";
    for (std::size_t i = 0; i < width + 2; i++){
        window += "─";
    }
    window += "┘";

    return window;
}

bool Choice::show(){
    bool continue_loop = true;
    do{
        std::cout << create_Window() << std::endl;
        Key c = Getkey::getkey();
        switch (c){
            case Key::ARROW_LEFT:{
                is_2_selected = false;
                break;
            }
            case Key::ARROW_RIGHT:{
                is_2_selected = true;
                break;
            }
            case Key::ENTER:{
                continue_loop = false;
                break;
            }
            case Key::ESCAPE:{
                erase();
                return false;
            }
            default:{
                break;
            }
        }
        erase();
    }while(continue_loop);
    return is_2_selected;
}

void Choice::erase(){
    std::cout << "\x1B[4A\x1B[0J" << std::flush;
}
